import * as types from './types.actions';
import * as mutation_types from './types.mutations';
import { upload } from '../services/file-upload.service';

export default {

  [types.ADD_FILE] ( context, payload ) {
    let includesType;
    upload(payload.file).then(img => {
      let data = {
        'type' : payload.typeName,
        'file' : img
      };
      let files = context.state.files;
      if(files.length > 0) {
        for(let i=0; i<files.length; i++) {
          if(files[i].type == payload.typeName) {
            includesType = 1;
            break;
          } else {
            includesType = 0;
          }
        }
        if(includesType == 1) {
          context.commit (mutation_types.M_CHANGE_FILE, {data});
        } else {
          context.commit (mutation_types.M_ADD_FILE, {data});
        }
      } else {
        context.commit (mutation_types.M_ADD_FILE, {data});
      }
    });
  },
  [types.DELETE_FILE_BY_ID] ( context, payload ) {
    context.commit (mutation_types.M_DELETE_FILE_BY_ID, {payload});
  },
  [types.UPLOAD_FILES] ( context, payload ) {
    //upload to server logic;
    context.commit (mutation_types.M_UPLOAD_FILES, {payload});
  },

}
