import * as mutation_types from './types.mutations';

export default {

  [mutation_types.M_ADD_FILE] ( state, payload ) {
    state.files.push(payload.data);
    console.log();
  },
  [mutation_types.M_CHANGE_FILE] ( state, payload ) {
    let files = state.files;
    files.map((item,index) => {
      if(item.type === payload.data.type) {
        state.files[index].file = payload.data.file;
      }
    });
  },
  [mutation_types.M_DELETE_FILE_BY_ID] ( state, payload ) {
    let id = payload.payload.id;
    state.files.splice(id, 1);
  },
  [mutation_types.M_UPLOAD_FILES] ( state, payload ) {
    let files = state.files;
    state.uploadedFiles = files;
    state.files = [];
  }
};

