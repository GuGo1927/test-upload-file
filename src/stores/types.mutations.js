export const M_ADD_FILE = "M_ADD_FILE";
export const M_CHANGE_FILE = "M_CHANGE_FILE";
export const M_DELETE_FILE_BY_ID = "M_DELETE_FILE_BY_ID";
export const M_UPLOAD_FILES = "M_UPLOAD_FILES";
