function upload(formData) {
  const photos = formData.getAll('photos');
  const promises = photos.map((x) => getImage(x)
    .then(img => ({
      id: img,
      originalName: x.name,
      fileName: x.name,
      url: img
    })));
  return promises[0];
}

function getImage(file) {
  return new Promise((resolve, reject) => {
    const fReader = new FileReader();
    const img = document.createElement('img');

    fReader.onload = () => {
      img.src = fReader.result;
      resolve(img);
    };

    fReader.readAsDataURL(file);
  })
}

export { upload, getImage }
